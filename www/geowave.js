var Geowave = function() {};

Geowave.prototype.initWithAppID = function(successCallback, failureCallback, appID) {
    cordova.exec(successCallback, failureCallback, "GeowavePlugin", "initWithAppID", [{appID: appID}]);
};

Geowave.prototype.initWithAppIDAndSenderID = function(successCallback, failureCallback, appID, senderID) {
    cordova.exec(successCallback, failureCallback, "GeowavePlugin", "initWithAppIDAndSenderID", [{appID: appID, senderID: senderID}]);
};

Geowave.prototype.requestLocationAuthorization = function(successCallback, failureCallback) {
    cordova.exec(successCallback, failureCallback, "GeowavePlugin", "requestLocationAuthorization", []);
};

// Geowave.prototype.requestNotificationsAuthorization = function(successCallback, failureCallback) {
//     cordova.exec(successCallback, failureCallback, "GeowavePlugin", "requestNotificationsAuthorization", []);
// };

Geowave.prototype.setPushSettings = function(successCallback, failureCallback, pushToken) {
    cordova.exec(successCallback, failureCallback, "GeowavePlugin", "setPushSettings", [{pushToken: pushToken}]);
};

Geowave.prototype.deviceIDFA = function(successCallback, failureCallback) {
    cordova.exec(successCallback, failureCallback, "GeowavePlugin", "deviceIDFA", []);
};

// Geowave.prototype.lastCampaignURL = function(successCallback, failureCallback) {
//     cordova.exec(successCallback, failureCallback, "GeowavePlugin", "lastCampaignURL", []);
// };
//
// Geowave.prototype.lastCampaignPayload = function(successCallback, failureCallback) {
//     cordova.exec(successCallback, failureCallback, "GeowavePlugin", "lastCampaignPayload", []);
// };

Geowave.prototype.createSegmentsWithName = function(successCallback, failureCallback, name, values) {
    cordova.exec(successCallback, failureCallback, "GeowavePlugin", "createSegmentsWithName", [{name: name, values: values}]);
};

Geowave.prototype.removeSegmentsWithName = function(successCallback, failureCallback, name, values) {
    cordova.exec(successCallback, failureCallback, "GeowavePlugin", "removeSegmentsWithName", [{name: name, values: values}]);
};

Geowave.prototype.removeAllSegments = function(successCallback, failureCallback) {
    cordova.exec(successCallback, failureCallback, "GeowavePlugin", "removeAllSegments", []);
};

Geowave.prototype.createFunnelWithName = function(successCallback, failureCallback, name, level) {
    cordova.exec(successCallback, failureCallback, "GeowavePlugin", "createFunnelWithName", [{name: name, level: level}]);
};

Geowave.prototype.removeAllFunnels = function(successCallback, failureCallback) {
    cordova.exec(successCallback, failureCallback, "GeowavePlugin", "removeAllFunnels", []);
};
	
module.exports = new Geowave();