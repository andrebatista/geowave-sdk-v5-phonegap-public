package com.rippll.geowavesdk;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;

import org.json.JSONArray;
import org.json.JSONObject;

import com.rippll.geowavesdk.Geowave;

public class GeowavePlugin extends CordovaPlugin {
    public static final String INIT_WITH_APPID_AND_SENDERID = "initWithAppIDAndSenderID";
    public static final String REQUEST_LOCATION_AUTHORIZATION = "requestLocationAuthorization";
    public static final String REQUEST_NOTIFICATIONS_AUTHORIZATION = "requestNotificationsAuthorization";

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        try {
            if (INIT_WITH_APPID_AND_SENDERID.equals(action)) {
                JSONObject object = args.getJSONObject(0);

                Geowave.initWithActivity(this.cordova.getActivity(), object.getString("appID"), object.getString("senderID"));

                Geowave.startUsingLocationServices();

                Geowave.startUsingPushNotifications();

                callbackContext.success();

                return true;
            }
            
            callbackContext.error("Invalid action");

            return false;
        } catch(Exception exception) {
            callbackContext.error(exception.getMessage());

            return false;
        }
    }
}